defmodule Diaryfishing.Repo.Migrations.CreateFishdays do
  use Ecto.Migration

  def change do
    create table(:fishdays) do
      add :name, :string
      add :latitude, :float
      add :longitude, :float
      add :fish_score, :float
      add :date, :naive_datetime
      add :hours, :integer
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:fishdays, [:user_id])
  end
end
