defmodule Diaryfishing.Repo.Migrations.AddMapToFishDay do
  use Ecto.Migration
  def change do
    alter table(:fishdays) do
      add :data, :map, default: %{}
    end
  end
end
