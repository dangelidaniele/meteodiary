defmodule Diaryfishing.Repo.Migrations.AddLocationToFishDays do
  use Ecto.Migration

  def change do
    alter table(:fishdays) do
      add :location, :string, default: "not available"
    end
  end
end
