defmodule Diaryfishing.Repo.Migrations.AddSessionCatches do
  use Ecto.Migration

  def change do
    create table(:catches, primary_key: false) do
      add(:fishday_id, references(:fishdays, on_delete: :delete_all), primary_key: true)
      add(:user_id, references(:user, on_delete: :delete_all), primary_key: true)
      timestamps()
    end

    create(index(:user_project, [:project_id]))
    create(index(:user_project, [:user_id]))

end
