'use strict';

import * as ChartAnnotation from "chartjs-plugin-annotation";
import {FishHour, FishHourCollection} from '../../fish/fishour';
import formatAMPM from "../../helpers/helpers";
import Chart from "chart.js";

const airTemperatures = function(){
    return {
        date: 'today',
        options: [],
        showDropdown: false,
        selectedOption: 0,
        selectOption: function(index){
            this.selectedOption = index;
            this.date = this.options[index].value;
            this.renderChart();
        },
        data: null,
        labels: null,
        waves: null,
        fetch: function(data){
            this.fishCollection = new FishHourCollection(
                data.data.weather.map(function (fishDayData) {
                    return new FishHour(fishDayData)
                })
            );
            this.hours = data.hours

            this.start = new Date(data.start);
            this.end = new Date(data.start).addHours(this.hours);


            this.options =[{
                label: formatAMPM(this.start) + " - " + formatAMPM(this.end),
                value: 1,
            }];

            this.renderChart();
        },
        renderChart: function(){
            let c = false;

            Chart.helpers.each(Chart.instances, function(instance) {
                if (instance.chart.canvas.id == 'chart-air-temperature') {
                    c = instance;
                }
            });

            if(c) {
                c.destroy();
            }

            let ctx = document.getElementById('chart-air-temperature').getContext('2d');

            let namedChartAnnotation = ChartAnnotation;
            let chart = new Chart(ctx, {
                type: "line",
                plugins: [namedChartAnnotation],
                data: {
                    labels: this.fishCollection.getTimeLabels(),
                    datasets: [
                        {
                            label: "Water Temperature",
                            backgroundColor: "rgba(102, 126, 234, 0.25)",
                            borderColor: "rgba(102, 126, 234, 1)",
                            pointBackgroundColor: "rgba(102, 126, 234, 1)",
                            pointStyle: "line",
                            data: this.fishCollection.WaterTemperatures(),
                            tension: "0.3",
                            borderWidth: 1.5
                        },
                        {
                            label: "Air Temperature",
                            backgroundColor: "rgba(40, 135, 234, 0.25)",
                            borderColor: "rgba(40, 135, 234, 1)",
                            pointBackgroundColor: "rgba(40, 135, 234, 1)",
                            pointStyle: "line",
                            data: this.fishCollection.AirTemperatures(),
                            tension: "0.3",
                            borderWidth: 1.5
                        }
                    ],
                },
                options: {
                    annotation: {
                        annotations: [
                            {
                                type: "box",
                                yScaleID: 'y-axis-0',
                                xScaleID: 'x-axis-0',
                                yMin: 0,
                                yMax: Math.max(Math.max.apply(null, this.fishCollection.AirTemperatures()), Math.max.apply(null, this.fishCollection.WaterTemperatures())) + 2,
                                xMin: this.start.getHours() - 1,
                                xMax: this.start.getHours() + this.hours -1 ,
                                borderWidth: 1,
                                backgroundColor: "rgba(0,200,79,0.3)",
                            },
                        ]
                    },
                    title: {
                        display: true,
                        text: 'Air/Water Temperatures 24h'
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                beginAtZero: true,
                            }
                        }],
                        xAxes: [{
                            display: true,
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                beginAtZero: true,
                            }
                        }]
                    },
                }
            });
        }
    }
};

export default airTemperatures