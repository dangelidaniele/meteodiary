'use strict';

import * as ChartAnnotation from "chartjs-plugin-annotation";
import {Moon} from "../../fish/moon";
import {FishHour, FishHourCollection} from '../../fish/fishour';
import formatAMPM from "../../helpers/helpers";
import Chart from "chart.js";

const chartDataPressures =  function(){
    return {
        date: 'today',
        options: [],
        showDropdown: false,
        selectedOption: 0,
        selectOption: function(index){
            this.selectedOption = index;
            this.date = this.options[index].value;
            this.renderChart();
        },
        data: null,
        labels: null,
        waves: null,
        fetch: function(data){
            this.fishCollection = new FishHourCollection(
                data.data.weather.map(function (fishDayData) {
                    return new FishHour(fishDayData)
                })
            );

            this.moon = new Moon(data.data.moon[0]);

            this.hours = data.hours;

            this.start = new Date(data.start);
            this.end = new Date(data.start).addHours(this.hours);


            this.options =[{
                label: formatAMPM(this.start) + " - " + formatAMPM(this.end),
                value: 1,
            }];

            this.renderChart();
        },
        renderChart: function(){
            let c = false;

            Chart.helpers.each(Chart.instances, function(instance) {
                if (instance.chart.canvas.id == 'chart-data-pressure') {
                    c = instance;
                }
            });

            if(c) {
                c.destroy();
            }

            let ctx = document.getElementById('chart-data-pressure').getContext('2d');

            let namedChartAnnotation = ChartAnnotation;
            let chart = new Chart(ctx, {
                type: "bar",
                plugins: [namedChartAnnotation],
                data: {
                    labels: this.fishCollection.getTimeLabels(),
                    datasets: [
                        {
                            label: "Pressure in bar",
                            backgroundColor: "#3e49e8",
                            data: this.fishCollection.Pressures()
                        }
                    ]
                },
                options: {
                    annotation: {
                        annotations: [
                            {
                                drawTime: 'afterDatasetsDraw',
                                type: "box",
                                yScaleID: 'y-axis-0',
                                xScaleID: 'x-axis-0',
                                yMin:  Math.min.apply(null, this.fishCollection.Pressures()),
                                yMax: Math.max.apply(null, this.fishCollection.Pressures()),
                                xMin: this.start.getHours() - 1,
                                xMax: this.start.getHours() + this.hours -1 ,
                                backgroundColor: "rgba(0,200,79,0.3)",
                            },
                            {
                                drawTime: "afterDatasetsDraw",
                                type: "line",
                                mode: "vertical",
                                scaleID: "x-axis-0",
                                value: this.moon.getSunSetDate().getHours(),
                                yMax: Math.max.apply(null, this.fishCollection.Pressures()),
                                borderWidth: 2,
                                borderColor: 'rgb(255, 99, 132)',
                                label: {
                                    content: "sunset",
                                    enabled: true,
                                }
                            },
                            {
                                drawTime: "afterDatasetsDraw",
                                type: "line",
                                mode: "vertical",
                                scaleID: "x-axis-0",
                                value: this.moon.getSunRiseDate().getHours(),
                                yMax: Math.max.apply(null, this.fishCollection.Pressures()),
                                borderWidth: 2,
                                borderColor: 'rgb(255, 99, 132)',
                                label: {
                                    content: "sunrise",
                                    enabled: true,
                                }
                            }
                        ]
                    },
                    legend: { display: true },
                    title: {
                        display: true,
                        text: 'Atmospheric Pressure over 24h'
                    }
                }
            });
        }
    }
};

export default chartDataPressures