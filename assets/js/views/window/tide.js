'use strict';

import * as ChartAnnotation from "chartjs-plugin-annotation";
import {Moon} from "../../fish/moon";
import {FishHour, FishHourCollection} from '../../fish/fishour';
import formatAMPM from "../../helpers/helpers";
import Chart from "chart.js";
import {Tide} from "../../fish/tide";

const chartDataTide = function(){
    return {
        date: 'today',
        options: [],
        showDropdown: false,
        selectedOption: 0,
        selectOption: function(index){
            this.selectedOption = index;
            this.renderChart();
        },
        data: null,
        labels: null,
        waves: null,
        fetch: function(data){

            this.tide = new Tide(data.data.tide);
            this.moon = new Moon(data.data.moon[0]);

            this.hours = data.hours;
            this.start = new Date(data.start);
            this.end = new Date(data.start).addHours(this.hours);


            this.options =[{
                label: formatAMPM(this.start) + " - " + formatAMPM(this.end),
                value: 1,
            }];

            this.renderChart();
        },
        renderChart: function(){
            let c = false;

            Chart.helpers.each(Chart.instances, function(instance) {
                if (instance.chart.canvas.id == 'chart-data-tide') {
                    c = instance;
                }
            });

            if(c) {
                c.destroy();
            }

            let ctx = document.getElementById('chart-data-tide').getContext('2d');

            let namedChartAnnotation = ChartAnnotation;
            console.log("tide",  this.tide.getTideData()[0]);

            let chart = new Chart(ctx, {
                type: "line",
                plugins: [namedChartAnnotation],
                data: {
                    labels: this.tide.getTimeLabels(),
                    datasets: [
                        {
                            label: "Tide",
                            pointStyle: "line",
                            backgroundColor: "rgba(102, 126, 234, 0.25)",
                            borderColor: "rgba(102, 126, 234, 1)",
                            pointBackgroundColor: "rgba(102, 126, 234, 1)",
                            data: this.tide.getTideData(),
                            borderWidth: 1.5
                        },
                    ],
                },
                options: {
                    annotation: {
                        annotations: [
                            {
                                type: 'box',
                                backgroundColor: 'rgba(0,150,0,0.02)',
                                borderColor: 'rgba(0,150,0,0.2)',
                                borderWidth: 1,
                                cornerRadius: 4,
                                xMin: (ctx) => min(ctx, 0, 'x') - 2,
                                yMin: (ctx) => min(ctx, 0, 'y') - 2,
                                xMax: (ctx) => max(ctx, 0, 'x') + 2,
                                yMax: (ctx) => max(ctx, 0, 'y') + 2
                            }
                        ]
                    },
                    title: {
                        display: true,
                        text: 'Tide 24h'
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            gridLines: {
                                display: true
                            },
                            ticks: {
                                beginAtZero: true,
                            }
                        }],
                        xAxes: [{
                            display: true,
                            gridLines: {
                                display: true
                            },
                            ticks: {
                                beginAtZero: true,
                            }
                        }]
                    },
                }
            });
        }
    }
};

export default chartDataTide