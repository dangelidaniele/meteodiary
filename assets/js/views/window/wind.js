'use strict';

import * as ChartAnnotation from "chartjs-plugin-annotation";
import {Moon} from "../../fish/moon";
import {FishHour, FishHourCollection} from '../../fish/fishour';
import formatAMPM from "../../helpers/helpers";
import Chart from "chart.js";

const chartDataWind = function(){
    return {
        date: 'today',
        options: [],
        showDropdown: false,
        selectedOption: 0,
        selectOption: function(index){
            this.selectedOption = index;
            this.renderChart();
        },
        data: null,
        labels: null,
        waves: null,
        fetch: function(data){

            this.fishCollection = new FishHourCollection(
                data.data.weather.map(function (fishDayData) {
                    return new FishHour(fishDayData)
                })
            );

            this.hours = data.hours;
            this.start = new Date(data.start);
            this.end = new Date(data.start).addHours(this.hours);


            this.options =[{
                label: formatAMPM(this.start) + " - " + formatAMPM(this.end),
                value: 1,
            }];

            this.renderChart();
        },
        renderChart: function(){
            let c = false;

            Chart.helpers.each(Chart.instances, function(instance) {
                if (instance.chart.canvas.id == 'chart-data-wind') {
                    c = instance;
                }
            });

            if(c) {
                c.destroy();
            }

            let ctx = document.getElementById('chart-data-wind').getContext('2d');

            let namedChartAnnotation = ChartAnnotation;
            let chart = new Chart(ctx, {
                type: "line",
                plugins: [namedChartAnnotation],
                data: {
                    labels: this.fishCollection.getTimeLabels(),
                    datasets: [
                        {
                            label: "Wind speed",
                            pointStyle: "line",
                            backgroundColor: "white",
                            borderColor: "rgba(102, 126, 234, 1)",
                            pointBackgroundColor: "rgba(102, 126, 234, 1)",
                            data: this.fishCollection.WindSpeeds(),
                            yMax: 10,
                            yMin: 0,
                            borderWidth: 1.5
                        },
                    ],
                },
                options: {
                    annotation: {
                        annotations: [
                            {
                                type: "box",
                                yScaleID: 'y-axis-0',
                                xScaleID: 'x-axis-0',
                                yMin: Math.min.apply(null, this.fishCollection.WindSpeeds()),
                                yMax: Math.max.apply(null, this.fishCollection.WindSpeeds()),
                                xMin: this.start.getHours() - 1,
                                xMax: this.start.getHours() + this.hours -1 ,
                                borderWidth: 1,
                                backgroundColor: "rgba(0,200,79,0.3)",
                            },
                        ]
                    },
                    title: {
                        display: true,
                        text: 'Wind speed 24h'
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                beginAtZero: true,
                            }
                        }],
                        xAxes: [{
                            display: true,
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                beginAtZero: true,
                            }
                        }]
                    },
                }
            });
        }
    }
};

export default chartDataWind