'use strict';

import {FishHour, FishHourCollection} from '../../fish/fishour';
import formatAMPM, {calculateWindRose} from "../../helpers/helpers";
import * as d3 from "d3";

const chartDataWindDirection = function(){
    return {
        options: [],
        showDropdown: false,
        selectedOption: 0,

        selectOption: function (index) {
            this.selectedOption = index;
            this.renderChart();
        },
        fetch: function (data) {
            this.hours = data.hours;
            this.start = new Date(data.start);
            this.end = new Date(data.start).addHours(this.hours);
            let fishhours = data.data.weather;
            let startTimeDate = new Date(data.start);
            startTimeDate.setMinutes(0);

            let endTimeDate = new Date(data.start).addHours(data.hours);


            this.fishHoursCollection = new FishHourCollection(fishhours.filter(function (f) {
                let d = new Date(f.time);
                if (d >= startTimeDate && d <= endTimeDate) {
                    return true
                }
            }).map(function (data) {return new FishHour(data)}));

            this.options = [{
                label: formatAMPM(this.start) + " - " + formatAMPM(this.end),
                value: 1,
            }];

            this.renderD3();
        },


        renderD3() {
            let width = 540;
            let height = 300;

            const svg = d3.select("#windrose");

            const margin = { top: 20, right: 0, bottom: 20, left: 0 };
            const innerRadius = 20;
            const chartWidth = width - margin.left - margin.right;
            const chartHeight = height - margin.top - margin.bottom;
            const outerRadius = Math.min(chartWidth, chartHeight) / 2;
            const g = svg
                .append("g")
                .attr("transform", `translate(${width / 2},${height / 2})`);

            const angle = d3.scaleLinear().range([0, 2 * Math.PI]);
            const radius = d3
                .scaleLinear()
                .range([innerRadius, outerRadius]);
            const x = d3
                .scaleBand()
                .range([0, 2 * Math.PI])
                .align(0);
            const y = d3
                .scaleLinear() // you can try scaleRadial but it scales differently
                .range([innerRadius, outerRadius]);
            const z = d3
                .scaleOrdinal()
                .range([
                    "#8e44ad",
                    "#4242f4",
                    "#42c5f4",
                    "#42f4ce",
                    "#42f456",
                    "#adf442",
                    "#f4e242",
                    "#f4a142",
                    "#f44242",
                ]);

            const windData = {
                direction:  this.fishHoursCollection.getWindDirections(),
                speed:      this.fishHoursCollection.getWindSpeeds(),
            };

            const windRoseData = calculateWindRose(windData);
            let data = windRoseData;
            let columns = ChartDefaultProps.columns;

            x.domain(data.map(d => d.angle));
            y.domain([
                0,
                d3.max(data, d => d.total) > 1
                    ? d3.max(data, d => d.total)
                    : 1,
            ]);
            z.domain(columns.slice(1));
            // Extend the domain slightly to match the range of [0, 2π].
            angle.domain([0, d3.max(data, (d, i) => i + 1)]);
            radius.domain([0, d3.max(data, d => d.y0 + d.y)]);
            const angleOffset = -360.0 / data.length / 2.0;
            g.append("g")
                .selectAll("g")
                .data(d3.stack().keys(columns.slice(1))(data))
                .enter()
                .append("g")
                .attr("fill", d => z(d.key))
                .selectAll("path")
                .data(d => d)
                .enter()
                .append("path")
                .attr(
                    "d",
                    d3
                        .arc()
                        .innerRadius(d => y(d[0]))
                        .outerRadius(d => y(d[1]))
                        .startAngle(d => x(d.data.angle))
                        .endAngle(d => x(d.data.angle) + x.bandwidth())
                        .padAngle(0.01)
                        .padRadius(innerRadius),
                )
                .attr("transform", () => `rotate(${angleOffset})`);
            const label = g
                .append("g")
                .selectAll("g")
                .data(data)
                .enter()
                .append("g")
                .attr("text-anchor", "middle")
                .attr(
                    "transform",
                    d =>
                        `rotate(${((x(d.angle) + x.bandwidth() / 2) * 180) /
                        Math.PI -
                        (90 - angleOffset)})translate(${outerRadius + 30},0)`,
                );
            label
                .append("text")
                // eslint-disable-next-line no-confusing-arrow
                .attr("transform", d =>
                    (x(d.angle) + x.bandwidth() / 2 + Math.PI / 2) %
                    (2 * Math.PI < Math.PI)
                        ? "rotate(90)translate(0,16)"
                        : "rotate(-90)translate(0,-9)",
                )
                .text(d => d.angle)
                .style("font-size", 14);
            g.selectAll(".axis")
                .data(d3.range(angle.domain()[1]))
                .enter()
                .append("g")
                .attr("class", "axis")
                .attr(
                    "transform",
                    d => `rotate(${(angle(d) * 180) / Math.PI})`,
                )
                .call(
                    d3
                        .axisLeft()
                        .scale(
                            radius
                                .copy()
                                .range([-innerRadius, -(outerRadius + 10)]),
                        ),
                );
            const yAxis = g.append("g").attr("text-anchor", "middle");
            const yTick = yAxis
                .selectAll("g")
                .data(y.ticks(5).slice(1))
                .enter()
                .append("g");
            yTick
                .append("circle")
                .attr("fill", "none")
                .attr("stroke", "gray")
                .attr("stroke-dasharray", "4,4")
                .attr("r", y);
            
            yTick
                .append("text")
                .attr("y", d => -y(d))
                .attr("dy", "-0.35em")
                .attr("x", () => -10)
                .text(y.tickFormat(10, "s"))
                .style("font-size", 14);
            const legend = g
                .append("g")
                .selectAll("g")
                .style("margin-left", '14px')
                .data(columns.slice(1).reverse())
                .enter()
                .append("g")
                .attr(
                    "transform",
                    (d, i) =>
                        `translate(${outerRadius + 0},${-outerRadius +
                        40 +
                        (i - (columns.length - 1) / 2) * 20})`,
                );
            legend
                .append("rect")
                .attr("width", 18)
                .attr("height", 18)
                .attr("fill", z)
                .attr("dx", "45px")
            legend
                .append("text")
                .attr("x", 24)
                .attr("y", 9)
                .attr("dy", "0.35em")
                .text(d => d)
                .style("font-size", 12);
            g.exit().remove();

        }

    }
};

export const ChartDefaultProps = {
    columns: [
        "angle",
        "0-1",
        "1-2",
        "2-3",
        "3-4",
        "4-5",
        "5-6",
        "6-7",
        "7+",
    ],
};

export default chartDataWindDirection

