'use strict';
import formatAMPM from '../helpers/helpers'

export class FishHour {
    constructor(data) {
        let textArray = [
            'cloudy',
            'sun',
            'rain',
        ];
        let randomNumber = Math.floor(Math.random()*textArray.length);

        
        this.time = new Date(data.time),
        this.id = formatAMPM(new Date(data.time)),
        this.humidity = parseFloat(data.humidity.meteo, 10),

        this.precipitation = parseFloat(data.precipitation.noaa, 10),
        this.cloudCover = parseFloat(data.cloudCover.noaa, 10),

        this.windSpeed = parseFloat(data.windSpeed.noaa, 10),
        this.windDirection = parseFloat(data.windDirection.noaa, 10),

        this.waveHeight = parseFloat(data.waveHeight.icon, 10),

        this.swellDirection = parseInt(data.swellDirection.meteo),
        this.swellPeriod = parseFloat(data.swellPeriod.meteo, 10),

        this.airTemperature = parseFloat(data.airTemperature.noaa, 10),
        this.waterTemperature = parseFloat(data.waterTemperature.noaa, 10),

        this.sky = textArray[randomNumber],
        this.pressure = parseFloat(data.pressure.noaa, 10)
    }

    getTime() {
        return this.time;
    }

    getPressure() {
        return this.pressure;
    }

    getSwellPeriod() {
        return this.swellPeriod;
    }

    getWaveHeight() {
        return this.waveHeight;
    }

    getWaterTemperature() {
        return this.waterTemperature;
    }

    getAirTemperature() {
        return this.airTemperature;
    }

    getWindSpeed() {
        return this.windSpeed;
    }

    getWindDirection() {
        return this.windDirection;
    }

    toMap() {
        return {
            time: formatAMPM(this.time),
            id: this.id,
            humidity: this.humidity,
            windSpeed: this.windSpeed,
            temperature: this.airTemperature,
            sky: this.sky,
            pressure: this.pressure,
            cloudCover: this.pressure,
            precipitation: this.pressure,
        }
    }

};


export class FishHourCollection {
    constructor(fishHours) {
        this.fishHours = fishHours;
    }

    filter(startTimeDate, endTimeDate) {
        this.fishHours = this.fishHours.filter(function (f) {
            let d = new Date(f.time);
            return d >= startTimeDate && d <= endTimeDate;
        })
    }

    all() {
        return this.fishHours
    }

    getWindDirections() {
        return this.fishHours.map(function (fishHour) {
            return fishHour.getWindDirection();
        })
    }

    getWindSpeeds() {
        return this.fishHours.map(function (fishHour) {
            return fishHour.getWindSpeed();
        })
    }

    AirTemperatures() {
        return this.fishHours.map(function (fishHour) {
            return fishHour.getAirTemperature();
        })
    }

    WaterTemperatures() {
        return this.fishHours.map(function (fishHour) {
            return fishHour.getWaterTemperature();
        })
    }

    WavesSeries() {
        return this.fishHours.map(function (fishHour) {
            return fishHour.getWaveHeight();
        })
    }

    WavesSeriesCasual() {
        return this.fishHours.map(function (fishHour) {
            return Math.random() * (0.7 - 0.1) + 0.1;
        })
    }

    Pressures() {
        return this.fishHours.map(function (fishHour) {
            return fishHour.getPressure();
        })
    }

    WindSpeeds() {
        return this.fishHours.map(function (fishHour) {
            return fishHour.getWindSpeed();
        })
    }

    WindDirections() {
        return this.fishHours.map(function (fishHour) {
            return fishHour.getWindDirection();
        })
    }

    getTimeLabels() {
        return this.fishHours.map(function (fishHour) {
            return formatAMPM(fishHour.getTime());
        })
    }

    toMap() {
        return this.fishHours.map(function (fishHour) {
            return fishHour.toMap()
        })
    }
};
