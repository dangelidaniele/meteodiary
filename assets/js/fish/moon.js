'use strict';
import formatAMPM from '../helpers/helpers'

export class Moon {
    constructor(data) {
        console.log(data);
        this.moons = data;
    }


    getSunRiseDate() {
        return new Date(this.moons.sunrise);
    }

    getSunSetDate() {
        return new Date(this.moons.sunset);
    }
};
