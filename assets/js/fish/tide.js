'use strict';
import formatAMPM from '../helpers/helpers'

export class Tide {
    constructor(data) {
        console.log(data);
        this.tides = data;
    }


    getTimeLabels() {
        return this.tides.map(function (t) {
            return formatAMPM(new Date(t.time));
        })
    }

    getTideData() {
        return this.tides.map(function (t) {
            return t.height;
        })
    }

};
