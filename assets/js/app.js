// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.css";
import "phoenix_html"
import './leaflet/leaflet-map'

import formatAMPM from './helpers/helpers'
import 'alpinejs'

import {FishHour, FishHourCollection} from './fish/fishour';
import chartDataWaves from "./views/window/waves";
import chartDataPressures from "./views/window/pressure";
import airTemperatures from "./views/window/airTemperatures";
import chartDataWind from "./views/window/wind";
import chartDataTide from "./views/window/tide";
import chartDataWindDirection from "./views/window/windDirection";


Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
};

Number.prototype.comma_formatter = function() {
    return this.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
};

window.chartDataWaves = function() {
    return chartDataWaves();
};

window.chartDataWind = function() {
    return chartDataWind();
};
window.chartDataTide = function() {
    return chartDataTide();
};

window.chartDataAirTemperatures = function() {
  return airTemperatures()
};
window.chartDataPressure = function() {
    return chartDataPressures();
};

window.chartDataWindDirection = function() {
    return chartDataWindDirection();
};


window.fishHours = function(){
    return {
        fishHours: [],
        where: "Terracina, Italia",

        fetch: function(data){
            let fishhours = data.data.weather;
            console.log(data)
            let startTimeDate = new Date(data.start);
            startTimeDate.setMinutes(0);

            let endTimeDate = new Date(data.start).addHours(data.hours);


            let fishourCollection = new FishHourCollection(fishhours.filter(function (f) {
                let d = new Date(f.time);
                if (d >= startTimeDate && d <= endTimeDate) {
                    return true
                }
            }).map(function (data) {return new FishHour(data)}));

            this.fishHours = fishourCollection.toMap()
        },

        location: function () {
            return this.where;
        }
    }
};

