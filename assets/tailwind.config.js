const plugin = require("tailwindcss/plugin");

module.exports = {
    important: true,
    // Active dark mode on class basis
    darkMode: "class",
    i18n: {
        locales: ["en-US"],
        defaultLocale: "en-US",
    },
    purge: {
        mode: "all",
        content: [
            "../**/*.html.eex",
            "../**/*.html.leex",
            "../**/views/**/*.ex",
            "../**/live/**/*.ex",
            "./js/**/*.js",
            "./pages/**/*.tsx",
            "./components/**/*.tsx"
        ],
        options: {},
        // These options are passed through directly to PurgeCSS
    },
    theme: {
        extend: {
            backgroundImage: (theme) => ({
                check: "url('/icons/check.svg')",
                landscape: "url('/images/landscape/2.jpg')",
            }),
        },
    },
    variants: {
        extend: {
            backgroundColor: ["checked"],
            borderColor: ["checked"],
            inset: ["checked"],
            zIndex: ["hover", "active"],
        },
    },
    plugins: [
        require('@tailwindcss/custom-forms'),
    ],
    future: {
        purgeLayersByDefault: true,
    },
};