use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :diaryfishing, DiaryfishingWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :diaryfishing, Diaryfishing.Repo,
  username: "daniele.dangeli",
  password: "",
  database: "diaryfishing_dev_test",
  hostname: "localhost",
  pool_size: 10,
  pool: Ecto.Adapters.SQL.Sandbox
