# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :diaryfishing,
  ecto_repos: [Diaryfishing.Repo]

# Configures the endpoint
config :diaryfishing, DiaryfishingWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "EeE9RgcREcolPpQF1VzdEvjIPV+W7lGBjdjBhOnMfAPICdys//MiUvhiWy3UbTaD",
  render_errors: [view: DiaryfishingWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Diaryfishing.PubSub, adapter: Phoenix.PubSub.PG2]


config :diaryfishing, :pow,
       user: Diaryfishing.Users.User,
       repo: Diaryfishing.Repo,
       extensions: [PowResetPassword, PowEmailConfirmation],
       controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks,
       web_module: DiaryfishingWeb,
       mailer_backend: DiaryfishingWeb.Pow.Mailer,
       web_module_mailer: DiaryfishingWeb

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
