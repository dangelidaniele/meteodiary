defmodule Diaryfishing.FishdayTest do
  use Diaryfishing.DataCase

  alias Diaryfishing.Fishday

  describe "fishdays" do
    alias Diaryfishing.Fishday.Fishdays

    @valid_attrs %{date: ~N[2010-04-17 14:00:00], fish_score: 120.5, hours: 42, latitude: 120.5, longitude: 120.5, name: "some name"}
    @update_attrs %{date: ~N[2011-05-18 15:01:01], fish_score: 456.7, hours: 43, latitude: 456.7, longitude: 456.7, name: "some updated name"}
    @invalid_attrs %{date: nil, fish_score: nil, hours: nil, latitude: nil, longitude: nil, name: nil}
    @ecto_user %Diaryfishing.Users.User{id: 1, email: "test@test.it"}

    def fishdays_fixture(attrs \\ %{}) do
      {:ok, fishdays} =
        Fishday.create_fishdays(@ecto_user, attrs |> Enum.into(@valid_attrs))

      fishdays
    end

    test "get_fishdays!/1 returns the fishdays with given id" do
      fishdays = fishdays_fixture()
      assert Fishday.get_fishdays!(fishdays.id).id == fishdays.id
    end

    test "create_fishdays/1 with valid data creates a fishdays" do
      assert {:ok, %Fishdays{} = fishdays} = Fishday.create_fishdays(@ecto_user, @valid_attrs)
      assert fishdays.date == ~N[2010-04-17 14:00:00]
      assert fishdays.fish_score == 120.5
      assert fishdays.hours == 42
      assert fishdays.latitude == 120.5
      assert fishdays.longitude == 120.5
      assert fishdays.name == "some name"
    end

    test "create_fishdays/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Fishday.create_fishdays(@ecto_user, @invalid_attrs)
    end

    test "update_fishdays/2 with valid data updates the fishdays" do
      fishdays = fishdays_fixture()
      assert {:ok, %Fishdays{} = fishdays} = Fishday.update_fishdays(fishdays, @update_attrs)
      assert fishdays.date == ~N[2011-05-18 15:01:01]
      assert fishdays.fish_score == 456.7
      assert fishdays.hours == 43
      assert fishdays.latitude == 456.7
      assert fishdays.longitude == 456.7
      assert fishdays.name == "some updated name"
    end

    test "update_fishdays/2 with invalid data returns error changeset" do
      fishdays = fishdays_fixture()
      assert {:error, %Ecto.Changeset{}} = Fishday.update_fishdays(fishdays, @invalid_attrs)
      assert fishdays.id == Fishday.get_fishdays!(fishdays.id).id
    end

    test "delete_fishdays/1 deletes the fishdays" do
      fishdays = fishdays_fixture()
      assert {:ok, %Fishdays{}} = Fishday.delete_fishdays(fishdays)
      assert_raise Ecto.NoResultsError, fn -> Fishday.get_fishdays!(fishdays.id) end
    end

    test "change_fishdays/1 returns a fishdays changeset" do
      fishdays = fishdays_fixture()
      assert %Ecto.Changeset{} = Fishday.change_fishdays(fishdays)
    end
  end
end
