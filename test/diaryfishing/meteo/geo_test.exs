defmodule Diaryfishing.Meteo.GeoTest do
  use Diaryfishing.DataCase

  alias Diaryfishing.Meteo.Geo

  describe "geo" do
    test "it should create empty geo nil coords" do
      geo = Geo.new(%{})
      assert geo != nil
      assert geo.latitude == nil
      assert geo.longitude == nil
    end

    test "it should create geo" do
      geo = Geo.new(%{latitude: 10.3, longitude: 33.5533})
      assert geo != nil
      assert geo.latitude == 10.3
      assert geo.longitude == 33.5533
    end

    test "it should approx latitude" do
      geo = Geo.new(%{latitude: 10.3333, longitude: 33.5533})
      assert geo != nil
      assert  Geo.latitude(geo) == 10.33
      assert  Geo.longitude(geo) == 33.55
    end
  end
end
