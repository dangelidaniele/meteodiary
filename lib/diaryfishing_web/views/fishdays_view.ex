defmodule DiaryfishingWeb.FishdaysView do
  use DiaryfishingWeb, :view

  def date_format(date), do: date_format date, "%d %b %Y"

  def date_format(d, format_string) do
    "#{d.day}/#{d.month}/#{d.year}"
  end
end
