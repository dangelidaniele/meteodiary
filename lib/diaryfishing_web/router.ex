defmodule DiaryfishingWeb.Router do
  use DiaryfishingWeb, :router
  use Pow.Phoenix.Router

  use Pow.Extension.Phoenix.Router,
      extensions: [PowResetPassword, PowEmailConfirmation]

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :protected do
    plug Pow.Plug.RequireAuthenticated,
         error_handler: Pow.Phoenix.PlugErrorHandler
  end

  scope "/" do
    pipe_through :browser

    pow_routes()
    pow_extension_routes()
  end

  scope "/", DiaryfishingWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/fish/day", PageController, :fishday
    resources "/fishdays", FishdaysController
    get "/dashboard", DashboardController, :index
  end

  scope "/protected", DiaryfishingWeb do
    pipe_through [:browser, :protected]


  end

  if Mix.env == :dev do
    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end

  # Other scopes may use custom stacks.
  # scope "/api", DiaryfishingWeb do
  #   pipe_through :api
  # end
end
