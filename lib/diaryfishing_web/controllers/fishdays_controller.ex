defmodule DiaryfishingWeb.FishdaysController do
  use DiaryfishingWeb, :controller
  alias Diaryfishing.Meteo.Geo

  alias Diaryfishing.Fishday
  alias Diaryfishing.Fishday.Fishdays

  def index(conn, _params) do
    fishdays = Pow.Plug.current_user(conn) |>  Fishday.list_fishdays()
    render(conn, "index.html", fishdays: fishdays)
  end

  def new(conn, _params) do
    changeset = Fishday.change_fishdays(%Fishdays{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"fishdays" => fishdays_params}) do
    user = Pow.Plug.current_user(conn)

    case Fishday.create_fishdays(user, fishdays_params) do
      {:ok, fishdays} ->

        conn
        |> put_flash(:info, "Fishdays created successfully.")
        |> redirect(to: Routes.fishdays_path(conn, :show, fishdays))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    fishdays = Fishday.get_fishdays!(id)
    render(conn, "show.html", fishdays: fishdays)
  end

  def edit(conn, %{"id" => id}) do
    fishdays = Fishday.get_fishdays!(id)
    changeset = Fishday.change_fishdays(fishdays)
    render(conn, "edit.html", fishdays: fishdays, changeset: changeset)
  end

  def update(conn, %{"id" => id, "fishdays" => fishdays_params}) do
    fishdays = Fishday.get_fishdays!(id)

    case Fishday.update_fishdays(fishdays, fishdays_params) do
      {:ok, fishdays} ->
        conn
        |> put_flash(:info, "Fishdays updated successfully.")
        |> redirect(to: Routes.fishdays_path(conn, :show, fishdays))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", fishdays: fishdays, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    fishdays = Fishday.get_fishdays!(id)
    {:ok, _fishdays} = Fishday.delete_fishdays(fishdays)

    conn
    |> put_flash(:info, "Fishdays deleted successfully.")
    |> redirect(to: Routes.fishdays_path(conn, :index))
  end
end
