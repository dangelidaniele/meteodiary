defmodule DiaryfishingWeb.DashboardController do
  use DiaryfishingWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

end
