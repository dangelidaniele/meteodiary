defmodule DiaryfishingWeb.PageController do
  use DiaryfishingWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def fishday(conn, _params) do
    render(conn, "fishday.html")
  end
end
