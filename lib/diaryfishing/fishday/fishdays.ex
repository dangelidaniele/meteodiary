defmodule Diaryfishing.Fishday.Fishdays do
  use Ecto.Schema
  import Ecto.Changeset
  alias Diaryfishing.Meteo.Geo
  alias Diaryfishing.Meteo.GeoDate
  alias Diaryfishing.Meteo.Fetcher

  schema "fishdays" do
    field :fish_score, :float
    field :date, :naive_datetime
    field :hours, :integer
    field :latitude, :float
    field :longitude, :float
    field :name, :string
    field :data, :map
    field :location, :string

    belongs_to :user, Diaryfishing.Users.User

    timestamps()
  end

  @doc false
  def changeset(fishdays, attrs) do
    fishdays
    |> cast(attrs, [:name, :latitude, :longitude, :fish_score, :date, :hours, :data, :location])
    |> validate_required([:name, :latitude, :longitude, :fish_score, :date, :hours])
    |> add_geo_data()
    |> add_geo_location()
  end

  @doc false
  def add_geo_location(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{latitude: latitude, longitude: longitude}} ->
        put_change(
          changeset,
          :location,
          Geo.reverse_geo(Geo.new(%{latitude: latitude, longitude: longitude}))
        )
      _ ->
        changeset
    end
  end

  @doc false
  def add_geo_data(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{latitude: latitude, longitude: longitude, date: date}} ->
        put_change(
          changeset,
          :data,
          Fetcher.fetch_meteo(
            GeoDate.new%{
              date: Timex.Timezone.convert(date, Timex.Timezone.local()),
              geo: Geo.new(%{latitude: latitude, longitude: longitude})
            }
          )
        )
      _ ->
        changeset
    end
  end
end
