defmodule Diaryfishing.Meteo.Storage do
  use GenServer
  @moduledoc false
  @table :points
  @name DMF_C

  ## Client API
  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, :ok, opts ++ [name: DMF_C])
  end

  @doc """
    Asynchronous call to cache a value at the provided key. Any key that can
    be used with ETS can be used, and will be evaluated using `==`.
  """
  def cache(key, val) do
    GenServer.cast(DMF_C, {:cache, key, val})
  end

  @doc """
    Sychronously reads the cache for the provided key. If no value is found,
    returns :not_found .
  """
  def read(key) do
    case :ets.lookup(@table, key) do
      [{^key, value} | _rest] -> [{key, value} | _rest]
      [] -> []
    end
  end

  @doc """
    Sychronously reads the cache for the provided key. If no value is found,
    invokes default_fn and caches the result. Note: in order to prevent congestion
    of the RequestCache process, default_fn is invoked in the context of the caller
    process.
  """
  def read_or_cache_default(key, default_fn) do
    case read(key) do
      :not_found ->
        value = default_fn.()
        cache key, value
        value
      value ->
        value
    end
  end

  def init(:ok) do
    generate_table()
    {:ok, %{}}
  end

  def handle_cast({:cache, key, val}, state) do
    # insert the value into the table
    :ets.insert(@table, {key, val})
    {:noreply, %{keys: key}}
  end

  def handle_cast({:delete, cache_key}, state) do
    :ets.delete(@table, cache_key)
    {:noreply, state}
  end

  defp generate_table do
    :ets.new(@table, [:bag, :named_table, :public])
  end
end
