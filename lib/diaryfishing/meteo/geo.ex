defmodule Diaryfishing.Meteo.Geo do
  alias Diaryfishing.Meteo.Geo
  defstruct latitude: nil, longitude: nil
  use ExConstructor

  @type t(latitude, longitude) :: %Geo{latitude: latitude, longitude: longitude}
  @typedoc """
      Type that represents Geo point as latitide and longitude
  """
  @type t :: %Geo{latitude: float, longitude: float}

  @spec latitude(Geo.t()) :: float
  def latitude(geo) do
     geo.latitude |> Float.round(2)
  end

  @spec longitude(Geo.t()) :: float
  def longitude(geo) do
     geo.longitude |> Float.round(2)
  end

  @spec reverse_geo(Geo.t()) :: String.t()
  def reverse_geo(geo) do
    case  OpenStreetMap.reverse(format: "json", lat: geo.latitude, lon: geo.longitude) do
      {:ok, :timeout} -> "not available"
      {:ok, map} -> fetch_address(map)
    end
  end

  defp fetch_address(map) do
    Enum.join([get_in(map, ["address", "country"]), ",", get_in(map, ["address", "county"])])
  end
end