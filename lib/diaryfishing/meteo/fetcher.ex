defmodule Diaryfishing.Meteo.Fetcher do
  alias Diaryfishing.Meteo.Storage
  @moduledoc false

  @near_point_threshold 20000
  alias Diaryfishing.Meteo.Geo
  alias Diaryfishing.Meteo.GeoDate
  alias Diaryfishing.Meteo.Meteo
  #{:ok, datetime} = DateTime.now("Etc/UTC")
  #geo_date = GeoDate.new%{date: datetime, geo: Geo.new%{latitude: 40.12, longitude: 12.7}}

  @spec fetch_meteo(GeoDate.t()) :: map()
  def fetch_meteo(geo_date = %GeoDate{}) do

    case Enum.filter(Storage.read(GeoDate.begin_of_day(geo_date)), fn {_, {geo, _}} -> distance(geo_date.geo, geo) < @near_point_threshold end) do
      [{_, {_, data}} | _] ->
        IO.inspect("from cache")
        data #cache ets hit
      [] -> fetch_meteo_from_api(geo_date)
    end
  end

  @spec fetch_meteo_from_api(GeoDate.t()) :: map()
  def fetch_meteo_from_api(geo_date = %GeoDate{}) do

    {weather, tide, moon} = Meteo.all_points(geo_date)
    weather_data =  %{"weather": weather, "tide": tide, "moon": moon}

    key = GeoDate.begin_of_day(geo_date)
    Storage.cache(key, {geo_date.geo, weather_data})
    weather_data
  end

  @spec distance(Geo.t(), Geo.t()) :: Float.t()
  defp distance(geo_from, geo_to) do
    distance = Geocalc.distance_between(
      [geo_from.latitude, geo_from.longitude],
      [geo_to.latitude, geo_to.longitude])
    distance
  end

end
