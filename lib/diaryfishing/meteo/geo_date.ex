defmodule Diaryfishing.Meteo.GeoDate do
  defstruct [:date, :geo]
  use ExConstructor

  alias Diaryfishing.Meteo.Geo
  alias Diaryfishing.Meteo.GeoDate

  @type t(date, geo) :: %GeoDate{date: date, geo: geo}
  @typedoc """
      Type that represents GeoDate struct
  """
  @type t :: % GeoDate{date: DateTime.t(), geo: Geo.t()}

  @spec begin_of_day(GeoDate.t()) :: Int.t()
  def begin_of_day(geo_date = %GeoDate{}) do
    geo_date.date |> Timex.Timezone.beginning_of_day() |> DateTime.to_unix
  end

  @spec end_of_day(GeoDate.t()) :: Int.t()
  def end_of_day(geo_date = %GeoDate{}) do
    geo_date.date |> Timex.Timezone.end_of_day() |> DateTime.to_unix
  end
end