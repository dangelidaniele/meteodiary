defmodule Diaryfishing.Meteo.Meteo do
  use HTTPoison.Base

  @expected_fields ~w(hours)

  alias Diaryfishing.Meteo.Geo
  alias Diaryfishing.Meteo.GeoDate

  def process_request_url(url) do
    "https://api.stormglass.io/v2" <> url
  end

  def process_response_body(body) do
    body
    |> Jason.decode!
    |> Enum.at(0)
    |> elem(1)

  end

  @spec all_points(GeoDate.t()) :: map()
  def all_points(%GeoDate{} = geo_date) do
    {weather_point(geo_date), tide_point(geo_date), moon_point(geo_date)}
  end

  defp weather_point(%GeoDate{} = geo_date) do
    {:ok, rsp} =
      "/weather/point?" <> URI.encode_query([lat: Geo.latitude(geo_date.geo), lng: Geo.longitude(geo_date.geo), start: GeoDate.begin_of_day(geo_date), end: GeoDate.end_of_day(geo_date), params: "waveHeight,airTemperature,swellDirection,swellPeriod,swellHeight,waterTemperature,pressure,waveDirection,windSpeed,windDirection,seaLevel,precipitation,cloudCover,humidity"])
      |> get(%{"Authorization": "fa70af68-7501-11eb-958b-0242ac130002-fa70afe0-7501-11eb-958b-0242ac130002"})
      rsp.body
  end

  #v2/tide/extremes/point?lat=41.30025&lng=13.02815&start=2021-02-21&end=2021-02-22
  defp tide_point(%GeoDate{} = geo_date) do
    {:ok, rsp} =
      "/tide/extremes/point?" <> URI.encode_query([lat: Geo.latitude(geo_date.geo), lng: Geo.longitude(geo_date.geo), start: GeoDate.begin_of_day(geo_date), end: GeoDate.end_of_day(geo_date)])
      |> get(%{"Authorization": "fa70af68-7501-11eb-958b-0242ac130002-fa70afe0-7501-11eb-958b-0242ac130002"})
    rsp.body
  end

  #https://api.stormglass.io/v2/astronomy/point?lat=41.30025&lng=13.02815&start=1614578435&end=1614592835
  defp moon_point(%GeoDate{} = geo_date) do
    {:ok, rsp} =
      "/astronomy/point?" <> URI.encode_query([lat: Geo.latitude(geo_date.geo), lng: Geo.longitude(geo_date.geo), start: GeoDate.begin_of_day(geo_date), end: GeoDate.end_of_day(geo_date)])
      |> get(%{"Authorization": "fa70af68-7501-11eb-958b-0242ac130002-fa70afe0-7501-11eb-958b-0242ac130002"})
    rsp.body
  end
end