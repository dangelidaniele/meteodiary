defmodule Diaryfishing.Fishday do
  @moduledoc """
  The Fishday context.
  """

  import Ecto.Query, warn: false
  alias Diaryfishing.Repo

  alias Diaryfishing.Fishday.Fishdays

  @doc """
  Returns the list of fishdays.

  ## Examples

      iex> list_fishdays()
      [%Fishdays{}, ...]

  """
  def list_fishdays(user) do
      Fishdays |> where([f], f.user_id == ^user.id) |> Repo.all()
  end

  @doc """
  Gets a single fishdays.

  Raises `Ecto.NoResultsError` if the Fishdays does not exist.

  ## Examples

      iex> get_fishdays!(123)
      %Fishdays{}

      iex> get_fishdays!(456)
      ** (Ecto.NoResultsError)

  """
  def get_fishdays!(id), do: Repo.get!(Fishdays, id)

  @doc """
  Creates a fishdays.

  ## Examples

      iex> create_fishdays(%{field: value})
      {:ok, %Fishdays{}}

      iex> create_fishdays(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_fishdays(user, attrs \\ %{}) do
    %Fishdays{}
    |> Fishdays.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:user, user)
    |> Repo.insert()
  end

  @doc """
  Updates a fishdays.

  ## Examples

      iex> update_fishdays(fishdays, %{field: new_value})
      {:ok, %Fishdays{}}

      iex> update_fishdays(fishdays, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_fishdays(%Fishdays{} = fishdays, attrs) do
    fishdays
    |> Fishdays.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a fishdays.

  ## Examples

      iex> delete_fishdays(fishdays)
      {:ok, %Fishdays{}}

      iex> delete_fishdays(fishdays)
      {:error, %Ecto.Changeset{}}

  """
  def delete_fishdays(%Fishdays{} = fishdays) do
    Repo.delete(fishdays)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking fishdays changes.

  ## Examples

      iex> change_fishdays(fishdays)
      %Ecto.Changeset{source: %Fishdays{}}

  """
  def change_fishdays(%Fishdays{} = fishdays) do
    Fishdays.changeset(fishdays, %{})
  end
end
