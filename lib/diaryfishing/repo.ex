defmodule Diaryfishing.Repo do
  use Ecto.Repo,
    otp_app: :diaryfishing,
    adapter: Ecto.Adapters.Postgres
end
